var cek_semua_praktikum = () => {
  // A
  for (var i = 0; i < 3; i++) {
    option = document.getElementById('a'+(i+1)+'_9');
    if (option !== null)
      option.checked = true;
  }
  // B
  for (var i = 0; i < 7; i++) {
    // Rand 7 - 9
    var value = Math.floor(Math.random() * 3) + 7;
    option = document.getElementById('b'+(i+1)+'_'+value);
      if (option !== null)
        option.checked = true;
  }
  // C
  for (var i = 0; i < 5; i++) {
    // Rand 7 - 9
    var value = Math.floor(Math.random() * 3) + 7;
    option = document.getElementById('c'+(i+1)+'_'+value);
      if (option !== null)
        option.checked = true;
  }
  // D
  for (var i = 0; i < 3; i++) {
    // Rand 7 - 9
    var value = Math.floor(Math.random() * 3) + 7;
    option = document.getElementById('d'+(i+1)+'_'+value);
      if (option !== null)
          option.checked = true;
  }
  // E
  for (var i = 0; i < 2; i++) {
    option = document.getElementById('e'+(i+1)+'_2');
      if (option !== null)
          option.checked = true;
  }
}
var cek_semua_teori = () => {
  // A
  for (var i = 0; i < 9; i++) {
    // Rand 7 - 9
    var value = Math.floor(Math.random() * 3) + 7;
    option = document.getElementById('a'+(i+1)+'_' + value);
      if (option !== null)
          option.checked = true;
  }
  // B
  for (var i = 9; i < 14; i++) {
    // Rand 7 - 9
    var value = Math.floor(Math.random() * 3) + 7;
    option = document.getElementById('b'+(i+1)+'_' + value);
      if (option !== null)
          option.checked = true;
  }
  // C
  for (var i = 0; i < 2; i++) {
    option = document.getElementById('e'+(i+1)+'_2');
      if (option !== null)
          option.checked = true;
  }
}

var cek_semua_kepuasan = () => {
  for (var i = 0; i < 11; i++) {
    option = document.getElementById('r' + (i+1) + 'a3');
      if (option !== null)
        option.checked = true;
  }
}

function autorun() {
    cek_semua_praktikum();
    cek_semua_teori();
    cek_semua_kepuasan();
}

setInterval(autorun, 1000);
